/*----------------------------------------------------------------------
 Peter Perrotta
 CS-280-101 Spring 2016
----------------------------------------------------------------------*/

#include <iostream>

#include "parseTree.h"
#include "p2lex.h"

extern int errorCount;

ParseTree::ParseTree(ParseTree* left, ParseTree* right) :
					 leftChild(left), rightChild(right) {
	nline = linenum;
}

int ParseTree::getLineNumber() {
	return nline;
}

ParseTree* ParseTree::getLeftChild() {
	return leftChild;
}

ParseTree* ParseTree::getRightChild() {
	return rightChild;
}

int ParseTree::isTString() {
	return 0;
}

int ParseTree::isIdentifier() {
	return 0;
}

int ParseTree::isPrint() {
	return 0;
}

int ParseTree::isSet() {
	return 0;
}

int ParseTree::isInteger() {
	return 0;
}

int ParseTree::isAddOp() {
	return 0;
}

int ParseTree::isMultiplyOp() {
	return 0;
}

std::string ParseTree::eval() {
	return "base";
}

void ParseTree::printInfo() {
	std::cout << "base" << std::endl;
}

void ParseTree::trav(std::map<std::string, std::string>& sym) {
	if( leftChild ){
		leftChild->trav(sym);
	}
	if( rightChild ) {
		rightChild->trav(sym);
	}
	this->usedBeforeSet(sym);
}

void ParseTree::usedBeforeSet(std::map<std::string,
									   std::string>& sym) {
	return;
}
/*--------------------------------------------------------------------
 CS-280-101 FALL 2016
 Peter Perrotta
 Program 2
--------------------------------------------------------------------*/

#include <iostream>
#include <cstdlib>
#include <string>
#include <cctype>
#include <map>
#include <iterator>
#include <vector>

#include "p2lex.h"

int linenum = 1;

/*------------------------------------------------------------------*/

namespace chr {
	static const std::vector<char> operators = {
		'*', '+', '[', ']'
	};
	
	static const std::map<TokenType, std::string> keywords = {
		{PRINT, "print"},
		{SET, "set"}
	};
	
	static const std::map<TokenType, std::string> names = {
		{ID, "id"},
		{STR, "str"},
		{INT, "int"},
		{PLUS, "plus"},
		{STAR, "star"},
		{LEFTSQ, "leftsq"},
		{RIGHTSQ, "rightsq"},
		{PRINT, "print"},
		{SET, "set"},
		{SC, "sc"},
		{LPAREN, "lparen"},
		{RPAREN, "rparen"},
		{DONE, "done"},
		{ERR, "err"}
	};
};

/*------------------------------------------------------------------*/

bool isoperator(const char c);
TokenType getOperator(const char c);
bool instring(const char c);
void skipws(std::istream* instream, char* c);
TokenType identify(std::string& lexeme);
Token getComment(std::istream* instream, char* c);
Token getIdentifier(std::istream* instream, char* c);
Token getInteger(std::istream* instream, char* c);
Token getString(std::istream* instream, char* c);
Token getOperator(std::istream* instream, char* c);

/*------------------------------------------------------------------*/

std::ostream& operator<<(std::ostream& out, const Token& t) {
	TokenType type = t.getTok();
	if (type == INT || type == STR || type == ID || type == ERR) {
		out << chr::names.at(t.tok) << "(" << t.lexeme << ")";
	} else {
		out << chr::names.at(t.tok);
	}
	return out;
}

/*--------------------------------------------------------------------
 Function getToken - tokenizer
 
 Note: all checks that are considered preliminary checks shall be
 single IF statements (not chained to ELSE IF statements). The purpose
 of the preliminary checks is to ensure that only valid characters can
 be analyzed by the "meat" of this function.
 
 End of statements are delimited by linefeeds, semicolons, and EOF bit
 
 Parameters:
	instream	[istream*]		pointer to input stream

 Return:
	[Token] the next token in the stream
*/
Token getToken(std::istream* instream) {
	char c;
	
	instream->get(c);
	
	/* repositions char to something that is not whitespace to
	   allow for extraction of the next token; whitespace should
	   not get past this point to avoid returning with an error */
	if (isspace(c)) {
		skipws(instream, &c);
	}
	
	if (!(instream->good())) {
		return Token(DONE, "");
	}
	
	switch (c) {
		case '/':
			return getComment(instream, &c);
			
		case '"':
			return getString(instream, &c);
			
		case '(':
			return Token(LPAREN, "(");
			
		case ')':
			return Token(RPAREN, ")");
			
		case ';':
			return Token(SC, ";");
			
		default:
			;
	}
	
	/* immediately following token is an integer */
	if (isdigit(c)) {
		return getInteger(instream, &c);
		
	/* immediately following token is an operator */
	} else if (isoperator(c)) {
		return Token(getOperator(c), std::string(1, c));
		
	/* immediately following token is an identifier */
	} else if (isalpha(c)) {
		return getIdentifier(instream, &c);
	}
	
	/* reaching this point signifies that a character that does not
	   belong to the language has been encountered */
	return Token(ERR, std::string(1, c));
}

/*------------------------------------------------------------------*/
bool isoperator(const char c) {
	for (char o : chr::operators) {
		if (c == o) {
			return true;
		}
	}
	return false;
}

/*--------------------------------------------------------------------
	Regular expression for operators:			[\[\]\+\*]+
*/
TokenType getOperator(const char c) {
	switch (c) {
		case '+':
			return PLUS;
		
		case '*':
			return STAR;
		
		case '[':
			return LEFTSQ;
		
		case ']':
			return RIGHTSQ;
		
		default:
			return ERR;
	}
}

/*--------------------------------------------------------------------
	Regular expression for whitespace:			[ \t\n\v\f\r]+
*/
void skipws(std::istream* instream, char* c) {
	while (isspace(*c) && instream->good()) {
		if (*c == '\n') {
			++linenum;
		}
		instream->get(*c);
	}
}

/*------------------------------------------------------------------*/

bool instring(const char c) {
	return c != '"' && c != '\n';
}

/*------------------------------------------------------------------*/

Token getComment(std::istream* instream, char* c) {
	std::string lexeme = "";
	while (*c != '\n' && instream->good()) {
		lexeme.append(1, *c);
		instream->get(*c);
	}
	
	instream->putback(*c);
	
	if (lexeme[1] != '/') {
		return Token(ERR, lexeme);
	} else {
		// an empty lexeme for an identifier is not allowed anywhere,
		// so it will resemble a comment for the sake of a retVal
		return Token(ID, "");
	}
	
}

/*--------------------------------------------------------------------
	Regular expression for integer literals:	[+-]?[0-9]+[0-9]*
*/
Token getInteger(std::istream* instream, char* c) {
	std::string lexeme = "";
	do {
		lexeme.append(1, *c);
		instream->get(*c);
	} while (isdigit(*c) && instream->good());
	
	instream->putback(*c);
	
	return Token(INT, lexeme);
}

/*--------------------------------------------------------------------
	Regular expression for identifiers:		[a-zA-Z][0-9a-zA-Z]*
*/
Token getIdentifier(std::istream* instream, char* c) {
	std::string lexeme = "";
	
	while (isalpha(*c) && instream->good()) {
		lexeme.append(1, *c);
		instream->get(*c);
	}
	
	instream->putback(*c);
	
	TokenType type = identify(lexeme);
	
	return Token(type, lexeme);
}

/*------------------------------------------------------------------*/

TokenType identify(std::string& lexeme) {
	for (auto const& it : chr::keywords) {
		if (lexeme.compare(it.second) == 0) {
			return it.first;
		}
	}
	return ID;
}

/*--------------------------------------------------------------------
	Regular expression for string literals:		"."
*/
Token getString(std::istream* instream, char* c) {
	std::string lexeme = "";
	do {
		lexeme.append(1, *c);
		instream->get(*c);
	} while (instring(*c) && instream->good());
	
	if (*c == '"') {
		lexeme.append(1, *c);
		return Token(STR, lexeme);
	} else {
		return Token(ERR, lexeme);
	}
}
/*----------------------------------------------------------------------
 Peter Perrotta
 CS-280-101 Spring 2016
 Program 3
----------------------------------------------------------------------*/

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <cstring>

#include "p2lex.h"
#include "parseTree.h"

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*--------------------------------------------------------------------*/
/*####################################################################*/

class Identifier : public ParseTree {
	private:
		Token token;
		std::string name;
		std::string value;
		int line;
	public:
		Identifier(const Token& t, int line);
		std::string getName();
		std::string getValue();
		int getLine();
		void setValue(std::string s);
		int isIdent();
		void printInfo();
};

class Integer : public ParseTree {
	private:
		Token token;
	public:
		Integer(const Token& t);
		int getInteger();
		void printInfo();
};

class TString : public ParseTree {
	private:
		Token token;
		int line;
	public:
		TString(const Token& t, int line);
		std::string getString();
		int getLine();
		int isTString();
		void printInfo();
};

class StatementList : public ParseTree {
	public:
		StatementList(ParseTree* left, ParseTree* right);
		void printInfo();
};

class AddOp : public ParseTree {
	public:
		AddOp(ParseTree* lhs, ParseTree* rhs);
		int plusCount();
		int isLeaf();
		void printInfo();
};

class MultiplyOp : public ParseTree {
	public:
		MultiplyOp(ParseTree* lhs, ParseTree* rhs);
		int starCount();
		int isLeaf();
		void printInfo();
};

class Substring : public ParseTree {
	private:
		std::string subThis;
	public:
		Substring(ParseTree* s, ParseTree* lhs, ParseTree* rhs);
		int isLeaf();
		void printInfo();
};

class CharAt : public ParseTree {
	private:
		std::string charAtThis;
	public:
		CharAt(ParseTree* s, ParseTree* opd);
		int isLeaf();
		int substrCount();
		void printInfo();
};

class Print : public ParseTree {
	private:
		ParseTree* expr;
	public:
		Print(ParseTree* e);
		int isLeaf();
		void printInfo();
};

class Set : public ParseTree {
	private:
		std::string identifier;
		ParseTree* expr;
	public:
		Set(std::string id, ParseTree* e);
		int isLeaf();
		void printInfo();
};

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*--------------------------------------------------------------------*/
/*####################################################################*/

Identifier::Identifier(const Token& t, int l) : ParseTree(), token(t) {
	name = t.getLexeme();
	line = l;
}

std::string Identifier::getName() {
	return name;
}

std::string Identifier::getValue() {
	return value;
}

void Identifier::setValue(std::string s) {
	value = s;
}

int Identifier::getLine() {
	return line;
}

int Identifier::isIdent() {
	return 1;
}

void Identifier::printInfo() {
	std::cout << "ID: " << token.getLexeme() << std::endl;
}

/*--------------------------------------------------------------------*/

Integer::Integer(const Token& t) : ParseTree(), token(t) {
	
}

int Integer::getInteger() {
	return std::stoi(token.getLexeme());
}

void Integer::printInfo() {
	std::cout << "INT: " << token.getLexeme() << std::endl;
}

/*--------------------------------------------------------------------*/

TString::TString(const Token& t, int l) : ParseTree(), token(t) {
	line = l;
}

std::string TString::getString() {
	return token.getLexeme();
}

int TString::getLine() {
	return line;
}

void TString::printInfo() {
	std::cout << "STR: " << token.getLexeme() << std::endl;
}

int TString::isTString() {
	return 1;
}

/*--------------------------------------------------------------------*/

StatementList::StatementList(ParseTree* left=0, ParseTree* right=0) :
											 ParseTree(left, right) {
	
}

void StatementList::printInfo() {
	std::cout << "Statement List Node" << std::endl;
}

/*--------------------------------------------------------------------*/

AddOp::AddOp(ParseTree* lhs, ParseTree* rhs) : ParseTree(lhs, rhs) {
	
}

int AddOp::plusCount() {
	return 1;
}

int AddOp::isLeaf() {
	return 0;
}

void AddOp::printInfo() {
	std::cout << "AddOp Node" << std::endl;
}

/*--------------------------------------------------------------------*/

MultiplyOp::MultiplyOp(ParseTree* lhs, ParseTree* rhs) :
								   ParseTree(lhs, rhs) {
	
}

int MultiplyOp::starCount() {
	return 1;
}

int MultiplyOp::isLeaf() {
	return 0;
}

void MultiplyOp::printInfo() {
	std::cout << "MultiplyOp Node" << std::endl;
}

/*--------------------------------------------------------------------*/

Substring::Substring(ParseTree* s, ParseTree* lhs, ParseTree* rhs=0) :
								  ParseTree(s, new CharAt(lhs, rhs)) {
	
}

int Substring::isLeaf() {
	return 0;
}

void Substring::printInfo() {
	std::cout << "Substring Node" << std::endl;
}

/*--------------------------------------------------------------------*/

CharAt::CharAt(ParseTree* rhs, ParseTree* lhs) : ParseTree(rhs, lhs) {
	
}

int CharAt::isLeaf() {
	return 0;
}

int CharAt::substrCount() {
	return 1;
}

void CharAt::printInfo() {
	std::cout << "CharAt Node" << std::endl;
}

/*--------------------------------------------------------------------*/

Print::Print(ParseTree* e) : ParseTree(e) {
	
}

int Print::isLeaf() {
	return 0;
}

void Print::printInfo() {
	std::cout << "Print Node" << std::endl;
}

/*--------------------------------------------------------------------*/

Set::Set(std::string id, ParseTree* e) : ParseTree(e), identifier(id) {
	
}

int Set::isLeaf() {
	return 0;
}

void Set::printInfo() {
	std::cout << "Set Node" << std::endl;
}

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*--------------------------------------------------------------------*/
/*####################################################################*/

namespace trk {
	static Token current;			// keeps track of current token
	static Token previous;

	static std::istream* in;		// input source
									// NOTE: only accessed by next()
									// should NEVER be changed
	static int errorCount = 0;		// global error count

	static std::map<std::string, int> idents;

	static int numPlusOps = 0;
	static int numStarOps = 0;
	static int numSubstrOps = 0;
};

/*--------------------------------------------------------------------*/

void next();
bool accept(TokenType t);
bool expect(TokenType t);

ParseTree* program(std::istream* input);
ParseTree* statementList();
ParseTree* statement();
ParseTree* expression();
ParseTree* term();
ParseTree* primary();
ParseTree* sstring();

std::vector<std::string> traverse(ParseTree* node);
void postorder(ParseTree* node, std::vector<std::string>& sErrors);
void preorder(ParseTree* node, std::vector<std::string>& sErrors);
void inorder(ParseTree* node, std::vector<std::string>& sErrors);

void lexicalError();
void parseError(std::string msg);
void semanticErrors(std::vector<std::string>& errs);

void printStats();

/*----------------------------------------------------------------------
 Entry point
*/
int main(int argc, char* argv[]) {
	
	std::istream* inp = &std::cin;
	std::ifstream inf;
	
	/* Resolve input source */
	if (argc == 2) {
		inf.open(argv[1]);
		inp = &inf;
	}
	
	program(inp);
	
	return 0;
}

/*----------------------------------------------------------------------
 Gets the next input token and stores in the global: current
*/
void next() {
	
	trk::previous = trk::current;
	
	trk::current = getToken(trk::in);
	
	if (trk::current.getTok() == ERR) {
		lexicalError();
	}
	
	// Must check to see if it is a comment, TokenType = ID, Lexeme = ""
	if (trk::current.getTok() == ID && trk::current.getLexeme().length() == 0) {
		while (trk::current.getTok() == ID
			   && trk::current.getLexeme().length() == 0) {
			trk::current = getToken(trk::in);
		}
	}
}

/*----------------------------------------------------------------------
 Determines if the token is of a specific type
*/
bool accept(TokenType t) {
	
	if (t == trk::current.getTok()) {
		next();
		return true;
	}
	return false;
}

/*----------------------------------------------------------------------
 Determines if the token encountered is to be expected next
*/
bool expect(TokenType t) {
	
	if (accept(t)) {
		return true;
	}
	parseError(std::string(std::to_string(linenum)) + std::string(": Syntax error, invalid statement"));
	return false;
}

/*----------------------------------------------------------------------
 Follows the following EBNF:
	<string> ::= STR [LEFTSQ <expr> [SC <expr>] RIGHTSQ]
*/
ParseTree* sstring() {
	
	TString* result = 0;
	std::string subMe;
	ParseTree* left = 0;
	ParseTree* right = 0;
	
	expect(STR);
	result = new TString(trk::previous, linenum);
	subMe = result->getString();
	
	if (accept(LEFTSQ)) {
		left = expression();
		if (accept(SC)) {
			right = expression();
		}
		expect(RIGHTSQ);
		return new Substring(result, left, right);
	} else {
		return result;
	}
}

/*----------------------------------------------------------------------
 Follows the following EBNF:
	<primary> ::= ID | INT | <string> | LPAREN <expr> RPAREN
*/
ParseTree* primary() {
	
	ParseTree* result = 0;
	
	if (accept(ID)) {
		result = new Identifier(trk::previous, linenum);
	} else if (accept(INT)) {
		result = new Integer(trk::previous);
	} else if (accept(LPAREN)) {
		result = expression();
		expect(RPAREN);
	} else {
		result = sstring();
	}
	
	return result;
}

/*----------------------------------------------------------------------
 Follows the following EBNF:
	<term> ::= <primary> {STAR <term>}
*/
ParseTree* term() {
	
	ParseTree* left = 0;
	ParseTree* right = 0;
	
	left = primary();
	if (accept(STAR)) {
		right = term();
		return new MultiplyOp(left, right);
	}
	
	return left;
}

/*----------------------------------------------------------------------
 Follows the following EBNF:
	<expr> ::= <term> {PLUS <expr>}
*/
ParseTree* expression() {
	
	ParseTree* left = 0;
	ParseTree* right = 0;
	
	left = term();
	if (accept(PLUS)) {
		right = expression();
		return new AddOp(left, right);
	}
	
	return left;
}

/*----------------------------------------------------------------------
 Follows the following EBNF:
	<stmt> ::= (PRINT | SET ID) <expression> SC
*/
ParseTree* statement() {
	
	ParseTree* result = 0;
	
	if (accept(PRINT)) {
		result = expression();
		expect(SC);
		return new Print(result);
	} else if (accept(SET)) {
		if (accept(ID)) {
			std::string id = trk::previous.getLexeme();
			result = expression();
			expect(SC);
			trk::idents[id] = linenum;
			return new Set(id, result);
		} else {
			parseError(std::string(std::to_string(linenum)) + std::string(": Syntax error, invalid statement"));
		}
	} else {
		parseError(std::string(std::to_string(linenum)) + std::string(": Syntax error, invalid statement"));
	}
}

/*----------------------------------------------------------------------
 Follows the following EBNF:
	<stmt_list> ::= <stmt> {<stmt>}
*/
ParseTree* statementList() {

	ParseTree* left = 0;
	ParseTree* right = 0;
	
	left = statement();
	if (!(trk::current.getTok() == DONE) && !(trk::current.getTok() == ERR)) {
		right = statementList();
	}
	
	ParseTree* result = new StatementList(left, right);
	
	return result;
}

/*----------------------------------------------------------------------
 Entry point from caller
 NO OTHER FUNCTIONS IN THIS FILE SHOULD BE CALLED FROM AN OUTSIDE SOURCE
 
 Follows the following EBNF:
	<program> ::= <stmt_list>
*/
ParseTree* program(std::istream* input) {
	trk::in = input;
	next();
	ParseTree* result = 0;
	result = statementList();
	
	// check end of file
	if (!(trk::current.getTok() == DONE)) {
		return 0;
	}
	
	std::vector<std::string> sErrors = traverse(result);
	printStats();
	semanticErrors(sErrors);
	
	return result;
}

/*+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=
%	The code below is for traversing the generated parse tree		   %
% 																	   %
+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=*/

std::vector<std::string> traverse(ParseTree* node=0) {
	std::vector<std::string> errs;
	postorder(node, errs);
	return errs;
}

void postorder(ParseTree* node, std::vector<std::string>& sErrors) {
	
	if (node == 0) {
		return;
	}
	postorder(node->getLeftChild(), sErrors);
	
	postorder(node->getRightChild(), sErrors);
	
	if (node->plusCount()) {
		++trk::numPlusOps;
	}
	
	if (node->starCount()) {
		++trk::numStarOps;
	}
	
	if (node->substrCount()) {
		++trk::numSubstrOps;
	}
	
	if (node->isIdent()) {
		Identifier* id = dynamic_cast<Identifier*>(node);
		if (trk::idents.count(id->getName()) == 0) {
			//Symbol x used without being set at line z
			sErrors.push_back("Symbol " + id->getName() + " used without being set at line " + std::string(std::to_string(id->getLine())));
		}
	} else if (node->isTString()) {
		TString* ts = dynamic_cast<TString*>(node);
		std::string s = ts->getString();
		//Empty string not permitted on line z
		if (s.length() <= 2) {
			int currentLine = linenum;
			sErrors.push_back("Empty string not permitted on line " + std::string(std::to_string(ts->getLine())));
		}
	}
}

void preorder(ParseTree* node, std::vector<std::string>& sErrors) {
	if (node == 0) {
		return;
	}
	
	preorder(node->getLeftChild(), sErrors);
	preorder(node->getRightChild(), sErrors);
}

void inorder(ParseTree* node, std::vector<std::string>& sErrors) {
	if (node == 0) {
		return;
	}
	inorder(node->getLeftChild(), sErrors);
	inorder(node->getRightChild(), sErrors);
}

/*----------------------------------------------------------------------
 Called when a parsing error is encountered
 Terminates the parse with exit status 2
*/
void parseError(std::string msg) {
	std::cout << msg << std::endl;
	exit(2);
}

/*----------------------------------------------------------------------
 Called when a lexical error is encountered
 Terminates the parse with exit status 1
*/
void lexicalError() {
	std::cout << linenum << ": Syntax error, illegal statement" << std::endl;
	exit(1);
}

void semanticErrors(std::vector<std::string>& errs) {
	for (std::string s : errs) {
		std::cout << s << std::endl;
	}
}

void printStats() {
	std::cout << "Count of + operators: " << trk::numPlusOps << std::endl;
	std::cout << "Count of * operators: " << trk::numStarOps << std::endl;
	std::cout << "Count of [] operators: " << trk::numSubstrOps << std::endl;
}

/*----------------------------------------------------------------------
 Peter Perrotta
 CS-280-101 Spring 2016
----------------------------------------------------------------------*/

#ifndef PARSETREE_H
#define PARSETREE_H

#include <map>

class ParseTree {
	private:
		ParseTree* leftChild;
		ParseTree* rightChild;
		int nline;
		
	public:
		ParseTree(ParseTree* left=0, ParseTree* right=0);
		int getLineNumber();
		ParseTree* getLeftChild();
		ParseTree* getRightChild();
		
		virtual int isTString();
		virtual int isIdentifier();
		virtual int isPrint();
		virtual int isSet();
		virtual int isInteger();
		virtual int isAddOp();
		virtual int isMultiplyOp();
		
		virtual void printInfo();
		virtual std::string eval();
		
		void trav(std::map<std::string, std::string>& sym);
		virtual void usedBeforeSet(std::map<std::string, std::string>& sym);
};

#endif
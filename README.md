# README #

This repository holds the source code for a lexical
analyzer, a recursive-descent parser, and an
interpreter for the small language.

### How do I get set up? ###

Compile all the .cpp files and presto! You have the
ability to use the language.

### Contribution guidelines ###

This project is for school; shoot me an email if you
see serious design issues/bugs/whatever. Contributions
are welcome but this project is really just for me to
learn more about this stuff.

### Who do I talk to? ###

pjp49@njit.edu

### About the language ###

It is a very simple language and admittedly not very
useful in the real world. The program was written based
on the following EBNF:
* <program> ::= <statement_list>
* <statement_list> ::= <statement> {<statement>}
* <statement> ::= (PRINT | SET ID) <expression> SC
* <expression> ::= <term> {PLUS <expr>}
* <term> ::= <primary> {STAR <term>}
* <primary> ::= ID | INT | <string> | LPAREN <expression> RPAREN
* <string> ::= STR [LEFTSQ <expression> [SC <expression>] RIGHTSQ]
PRINT, SET, ID, INT, STR, etc. are terminals. Whitespace is ignored
but lines are counted by the number or newlines (starts at 1).
The follwoing rules apply to terminals (as regular expressions):
* INT:   [+-]?[0-9]+[0-9]*
* ID:    [a-zA-Z][0-9a-zA-Z]*
* STR:   "."
* Whitespace (ignored): [ \t\n\v\f\r]+
* Operators: [\[\]\+\*]+
* Comments: //.\n